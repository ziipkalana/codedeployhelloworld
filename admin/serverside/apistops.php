<?php

include "../../1dependencies/database.php";
header('Content-Type: application/json');

$uid = $_POST['uid'];
$hashString = $_POST['hash'];

$hashValid = validateAdminHash($uid, $hashString );

    if ($hashValid == true) {
  
  
	
			$jsonArray = array();
			
			$stopsArray = array();
			
			$query2    = 'SELECT * FROM Stops ORDER BY StopCreated DESC';
			$stopsSessions = $conn->query($query2);
			
			while ($row3 = $stopsSessions->fetch_object()) {
			
				$stop = array();
				$stop = getStop($row3->idStop);
				$stopsArray[] = $stop;
			}
			
			
			$jsonArray['stops'] = $stopsArray;
			$jsonArray['responseCode'] = 200;
			$jsonArray['responseMessage'] = 'Stops List';
			echo json_encode($jsonArray, JSON_UNESCAPED_SLASHES);
		
    } else {

		$jsonArray = array();
		$jsonArray['responseCode'] = 0;
		$jsonArray['responseMessage'] = 'Invalid hash';
		echo json_encode($jsonArray, JSON_UNESCAPED_SLASHES);
		
    
    }

	
	$conn->close();


?>