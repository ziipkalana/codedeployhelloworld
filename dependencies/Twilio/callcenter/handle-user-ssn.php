<?php
	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="UTF-8"?>';

	echo '<Response>';

	$user_pushed = (int) $_REQUEST['Digits'];
	
	$year = $_GET['year'];
	$month = $_GET['month'];
	$day = $_GET['day'];
	
	
	$data = array("uid" => "9", "month" => $month, "day" => $day, "year" => $year, "ssndigits" => $user_pushed);    
  
 // echo  json_encode($data);;

                                                                
$data_string = json_encode($data);                                                                                   
 
$ch = curl_init('http://api.myfidem.com/api/getAllPatiensByDate');                                                                      
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($data_string))                                                                       
);                                                                                                                   
 
$result = curl_exec($ch);

$obj = json_decode($result,true);

if ($obj[responseCode] == 200) {
	
	$counter = 0;
	echo '<Say >Select your patient record</Say>';
	
	echo '<Gather action="handle-user-patient.php?year='.$year.'&amp;month='.$month.'&amp;day='.$day.'&amp;ssn='.$user_pushed.'" finishOnKey="#" >';
	
	foreach($obj[posts] as $p)
										{
		echo '<Say>Are you '.$p[firstname].' '.$p[lastname].' with doctor '.$p[doctorname].'? Select this record by pressing number '.$counter.' key and the pound key.</Say>';
		
	$counter += 1;
		}
		echo'</Gather>';
	
} else {
	echo '<Say >Patient id is not correct. Taking you back to the main menu</Say>';
		echo '<Redirect>handle-incoming-call.xml</Redirect>';
}

		
	
	echo '</Response>';
?>
