<?php
	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="UTF-8"?>';

	echo '<Response>';

	# @start snippet
	$user_pushed = (int) $_REQUEST['Digits'];
	
	$docid = $_GET['docid'];
	$patientid = $_GET['patientid'];
	$medid = $_GET['medid'];
	# @end snippet

	if ($user_pushed == 1)
	{
		
	
		$data2 =  array("uid" => $docid, "patientid" => $patientid, "medid" => $medid );    
  
 		// echo  json_encode($data);;

                                                                
		$data_string2 = json_encode($data2);                                                                                   
 
		$ch2 = curl_init('http://api.myfidem.com/api/addRefill');                                                                      
		curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch2, CURLOPT_POSTFIELDS, $data_string2);                                                                  
		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch2, CURLOPT_HTTPHEADER, array(                                                                          
  		  'Content-Type: application/json',                                                                                
  		  'Content-Length: ' . strlen($data_string2))                                                                       
		);                                                                                                                   
 
		$result2 = curl_exec($ch2);
		
		$obj = json_decode($result2,true);


	
		if ($obj[responseCode] == 200) {
			echo '<Say >Refill request sent</Say>';
			echo '<Hangup />';
		
		} else {
			echo '<Say  >Something went wrong. Try again later</Say>';
			echo '<Hangup />';
		
		}
	} else {
		echo '<Say >Taking you back to the main menu</Say>';
		echo '<Redirect>handle-incoming-call.xml</Redirect>';
	}

	echo '</Response>';
?>
