<?php
	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="UTF-8"?>';

	echo '<Response>';

	$user_pushed = (int) $_REQUEST['Digits'];

	if ($user_pushed == 1)
	{
		echo '<Gather action="handle-extension.php" numDigits="1">';
		echo "<Say>Press 1 to refill using prescription i.d.</Say>";
		echo '<Say>Press 2 to refill using patient i.d.</Say>';
		echo '<Say>Press 3 to refill using your date of birth and last 4 digits of your social security number</Say>';
		echo '</Gather>';
		echo "<Say>Sorry, I didn't get your response.</Say>";
		echo '<Redirect method="GET">handle-user-input.php?Digits=1</Redirect>';
	}
	# @start snippet
	else if ($user_pushed == 2)
	{
		
		
		echo '<Say>Connecting you to agent 2.</Say>';
		echo '<Dial record="true"><Number url="screen-caller.xml">+14072745188</Dial></Number>';
	}
	# @end snippet
	else {
		// We'll implement the rest of the functionality in the 
		// following sections.
		echo "<Say>Sorry, I can't do that yet.</Say>";
		echo '<Redirect>handle-incoming-call.php</Redirect>';
	}

	echo '</Response>';
?>
