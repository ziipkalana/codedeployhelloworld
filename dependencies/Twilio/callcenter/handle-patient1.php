<?php
	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="UTF-8"?>';

	

	# @start snippet
	$user_pushed = (int) $_REQUEST['Digits'];
	# @end snippet
	                                                            
$data = array( "uid" => "9", "patientid" => $user_pushed);    
                                                                  
$data_string = json_encode($data);                                                                                   
 
$ch = curl_init('http://api.myfidem.com/api/getPatientProfile');                                                                      
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($data_string))                                                                       
);                                                                                                                   
 
$result = curl_exec($ch);

$obj = json_decode($result,true);

echo '<Response>';

if ($obj[responseCode] == 200) {
	echo '<Gather action="handle-medication3.php?docid='.$obj[posts][0][docid].'&amp;patientid='.$user_pushed.'" numDigits="1">';
		echo '<Say>Are you '.$obj[posts][0][firstname].' '.$obj[posts][0][lastname].'? Please confirm by pressing number 1 key</Say>';
		echo'</Gather>';
	
} else {
	echo '<Say >Patient id is not correct. Taking you back to the main menu</Say>';
		echo '<Redirect>handle-incoming-call.xml</Redirect>';
}


	
	echo '</Response>';
?>
