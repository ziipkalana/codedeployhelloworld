<?php
	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="UTF-8"?>';

	echo '<Response>';

	# @start snippet
	$user_pushed = (int) $_REQUEST['Digits'];
	# @end snippet

	if ($user_pushed == 1)
	{
		echo '<Gather action="handle-medication2.php" finishOnKey="#" >';
		echo '<Say >Please enter your medication i.d. and press the pound key to proceed</Say>';
		echo'</Gather>';
		echo '<Say >Sorry, I did not get your response. Going back to the main menu</Say>';
		echo '<Redirect>handle-incoming-call.xml</Redirect>';
	}
	# @start snippet
	else if ($user_pushed == 2)
	{
		echo '<Gather action="handle-patient1.php" finishOnKey="#" >';
		echo '<Say>Please enter your patient i.d. and press the pound key to proceed</Say>';
		echo'</Gather>';
		echo '<Say >Sorry, I did not get your response. Going back to the main menu</Say>';
		echo '<Redirect>handle-incoming-call.xml</Redirect>';
	}
	else if ($user_pushed == 3)
	{
		echo '<Gather action="handle-user-year.php" finishOnKey="#" >';
		echo '<Say>Please enter your birth year and press the pound key</Say>';
		echo'</Gather>';
		echo '<Say >Sorry, I did not get your response. Going back to the main menu</Say>';
		echo '<Redirect>handle-incoming-call.xml</Redirect>';
	}
	
	# @end snippet
	else if ($user_pushed == 3)
	{
		echo '<Say >Connecting you to agent 2.</Say>';
		echo '<Dial record="true"><Number url="screen-caller.xml">+14072745188</Dial></Number>';
	}
	else {
		echo '<Say>Taking you back to the main menu</Say>';
		echo '<Redirect>handle-incoming-call.xml</Redirect>';
	}

	echo '</Response>';
?>
